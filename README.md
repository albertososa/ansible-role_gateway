# Ansible role: gateway

Deploys a simple gateway (WAN/LAN) using iptables.

## Requirements

None.

## Role variables

Available variables are listed below, along with the default values (see `defaults/main.yml`):

```yaml
wan_interface: eth0
```

Set the name of the WAN interface.

```yaml
lan_interface: eth1
```

Set the name of the LAN interface.

```yaml
private_subnet: 192.168.1.0/24
```

Set the private subnet in CIDR notation.

## Author

Alberto Sosa (info@albertososa.es)
